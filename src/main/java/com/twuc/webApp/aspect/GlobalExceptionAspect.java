package com.twuc.webApp.aspect;

import com.twuc.webApp.domain.view.Message;
import org.apache.tomcat.util.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionAspect.class);

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Message> handlerIllegalArgumentException(IllegalArgumentException e) {
        LOGGER.error(e.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(new Message(e.getMessage()));
    }

}
